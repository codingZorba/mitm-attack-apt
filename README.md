
# MITM-attack-apt
The repository contains code that attempts to launch man-in-the middle attack on apt/apt-get

### Requirements:
```
python2.7
scapy
netfilterqueue
```

### How to run:
1. In order to launch man-in-the middle attack run scapymitm.py
```
Usage :python scapymitm.py <interface> <vicitm-ip> <gateway-ip> 
```

2. Next run scapymanipulate.py to modify the http packets

3. Execute install.sh for apt operations

### Authors:
Akash A and Yadnesh Salvi-(M.Tech(CSE) at IISc,Banagalore)



PS:Make sure you specify the correct interfaces in the code before running them!!!


